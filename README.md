# **Le projet CodeDuez**

![logo](codeduez.png)

+ ## **Le site du projet :** [CodeDuez](https://darksathilili.go.yj.fr/)
+ ## **Le flyer du projet :** [Flyer](CodeDuez.pdf)
+ ## **Les projets :**
    - **Projet 1 :**  [Création d’un site internet pour le lycée Paul-Duez de Cambrai](projets/projet1.pdf)
    - **Projet 2 :**  [Création d’une application type qualité de l’environnement](projets/projet2.pdf)
    - **Projet 3 :**  [Vidéos de présentation des filières du lycée](projets/projet3.pdf)
    - **Projet 4 :**  [Création d’une application jeu d’entraînement à différentes disciplines](projets/projet4.pdf)
    - **Projet 5 :**  [Création d’un jeu vidéo accessible aux personnes souffrant d’un handicap](projets/projet5.pdf)

<img src="https://www.pabloyglesias.com/wp-content/uploads/2013/04/creative-commons.gif" width="100">
